package com.demo.extension.config;

import com.vaadin.flow.router.Route;
import io.quarkus.arc.deployment.AdditionalBeanBuildItem;
import io.quarkus.arc.deployment.BeanDefiningAnnotationBuildItem;
import io.quarkus.deployment.annotations.BuildProducer;
import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;
import io.quarkus.deployment.builditem.nativeimage.ReflectiveClassBuildItem;
import io.quarkus.undertow.deployment.ServletBuildItem;
import org.jboss.jandex.DotName;

import static com.demo.extension.config.ServiceFuntions.routeClasses;
public class VaadinProcessor {

  private static DotName routeAnnotation = DotName.createSimple(Route.class.getName());

  @BuildStep
  FeatureBuildItem featureBuildItem() {
    return new FeatureBuildItem("quarkus-vaadin");
  }

  @BuildStep
  BeanDefiningAnnotationBuildItem registerRouteAnnotation() {
    return new BeanDefiningAnnotationBuildItem(routeAnnotation);
  }

  @BuildStep
  AdditionalBeanBuildItem registerVaadinServlet() {
    //TODO make it dynamic
    return new AdditionalBeanBuildItem(QuarkusVaadinServlet.class);
  }

  @BuildStep
  ServletBuildItem vaadinServletBuildItem() {
    //TODO make it dynamic
    return ServletBuildItem.builder(QuarkusVaadinServlet.class.getSimpleName(), QuarkusVaadinServlet.class.getName())
                           .addMapping("/*")
                           .build();
  }

  @BuildStep
  void scanForBeans(BuildProducer<ReflectiveClassBuildItem> reflectiveClass) {
    System.out.println("ttttttttttttttttttt " + reflectiveClass);
    routeClasses().get()
                  .stream()
                  .map(c -> c.getAnnotation(Route.class))
                  .map(Route::value)
                  .peek(v -> System.out.println("ggggggggggggggg3gg"+v))
                  .forEach(value -> reflectiveClass.produce(new ReflectiveClassBuildItem(false, false, value)));

  }


  //todo replace with reflections8
  @BuildStep
  void reflection(BuildProducer<ReflectiveClassBuildItem> reflectiveClass) {
    System.out.println("GggggggggggggggggggggG: " + reflectiveClass.getClass());
    // Vaadin
    ReflectiveClassBuildItem vaadinClassBuildItem = ReflectiveClassBuildItem.builder(
              "com.vaadin.flow.component.UI",
              "com.vaadin.flow.component.PollEvent",
              "com.vaadin.flow.component.ClickEvent",
              "com.vaadin.flow.component.CompositionEndEvent",
              "com.vaadin.flow.component.CompositionStartEvent",
              "com.vaadin.flow.component.CompositionUpdateEvent",
              "com.vaadin.flow.component.KeyDownEvent",
              "com.vaadin.flow.component.KeyPressEvent",
              "com.vaadin.flow.component.KeyUpEvent",
              "com.vaadin.flow.component.splitlayout.GeneratedVaadinSplitLayout$SplitterDragendEvent",
              "com.vaadin.flow.component.details.Details$OpenedChangeEvent",
              "com.vaadin.flow.component.details.Details",
              "com.vaadin.flow.router.InternalServerError",
              "com.vaadin.flow.router.RouteNotFoundError",
              "com.vaadin.flow.theme.lumo.Lumo",
              "com.vaadin.flow.component.Key",
              "com.vaadin.flow.component.button.Button",
              "com.vaadin.flow.component.button.ButtonVariant",
              "com.vaadin.flow.component.dependency.CssImport",
              "com.vaadin.flow.component.notification.Notification",
              "com.vaadin.flow.component.orderedlayout.VerticalLayout",
              "com.vaadin.flow.component.textfield.TextField",
              "com.vaadin.flow.router.Route",
              "com.vaadin.flow.server.PWA"
            )
                                                                            .constructors(true)
                                                                            .methods(true)
                                                                            .build();

    reflectiveClass.produce(vaadinClassBuildItem);
//    // Athmosphere
//    ReflectiveClassBuildItem athmosClassBuildItem = ReflectiveClassBuildItem.builder(
//        "org.atmosphere.cpr.DefaultBroadcaster", "org.atmosphere.cpr.DefaultAtmosphereResourceFactory",
//        "org.atmosphere.cpr.DefaultBroadcasterFactory", "org.atmosphere.cpr.DefaultMetaBroadcaster",
//        "org.atmosphere.cpr.DefaultAtmosphereResourceSessionFactory", "org.atmosphere.util.VoidAnnotationProcessor",
//        "org.atmosphere.cache.UUIDBroadcasterCache", "org.atmosphere.websocket.protocol.SimpleHttpProtocol",
//        "org.atmosphere.interceptor.IdleResourceInterceptor", "org.atmosphere.interceptor.OnDisconnectInterceptor",
//        "org.atmosphere.interceptor.WebSocketMessageSuspendInterceptor",
//        "org.atmosphere.interceptor.JavaScriptProtocol", "org.atmosphere.interceptor.JSONPAtmosphereInterceptor",
//        "org.atmosphere.interceptor.SSEAtmosphereInterceptor",
//        "org.atmosphere.interceptor.AndroidAtmosphereInterceptor",
//        "org.atmosphere.interceptor.PaddingAtmosphereInterceptor", "org.atmosphere.interceptor.CacheHeadersInterceptor",
//        "org.atmosphere.interceptor.CorsInterceptor")
//                                                                            .constructors(true)
//                                                                            .methods(true)
//                                                                            .build();
//
//    reflectiveClass.produce(athmosClassBuildItem);
  }


}

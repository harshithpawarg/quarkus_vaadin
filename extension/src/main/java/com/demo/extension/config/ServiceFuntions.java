package com.demo.extension.config;

import com.vaadin.flow.router.Route;

import java.util.Set;
import java.util.function.Supplier;

import static java.util.Collections.unmodifiableSet;

public interface ServiceFuntions {

  String DEFAULT_PKG_TO_SCAN          = "com.demo";
  String DEFAULT_PKG_TO_SCAN_PROPERTY = "vaadin-pkg-to-scan";

  static Supplier<Set<Class<?>>> routeClasses() {
    return () -> unmodifiableSet(new org.reflections8.Reflections(
        System.getProperty(DEFAULT_PKG_TO_SCAN_PROPERTY, DEFAULT_PKG_TO_SCAN)).getTypesAnnotatedWith(Route.class));
  }


}
